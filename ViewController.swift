//
//  ViewController.swift
//  FibonacciApp
//
//  Created by Mo Lotfi on 03/06/15.
//  Copyright (c) 2015 Mo Lotfi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var fibonacciSequence = FibonacciSequence(numberOfItemsInSequence: 2, includesZero: true)

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var includeZeroSwitch: UISwitch!
    @IBOutlet weak var includeZeroLabel: UILabel!
    @IBOutlet weak var numberOfItemLabel: UILabel!
    @IBOutlet weak var numberOfItemSlider: UISlider!
   

    override func viewDidLoad() {

        self.updateFibonacciSequence()
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func updateFibonacciSequence() {
        if includeZeroSwitch.on {
            numberOfItemSlider.maximumValue = 93
        } else {
            numberOfItemSlider.maximumValue = 92
        }
        fibonacciSequence = FibonacciSequence (numberOfItemsInSequence: Int(numberOfItemSlider.value), includesZero: includeZeroSwitch.on)
        textView.text = ("\(fibonacciSequence.values)")
        numberOfItemLabel.text = String(Int(numberOfItemSlider.value))
        if includeZeroSwitch.on {
        includeZeroLabel.text = "Yesy"
        } else {
        includeZeroLabel.text = "No"}
        
    }

}
