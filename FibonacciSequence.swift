//
//  FibonacciSequence.swift
//  FibonacciApp
//
//  Created by Mo Lotfi on 03/06/15.
//  Copyright (c) 2015 Mo Lotfi. All rights reserved.
//

import Foundation

class FibonacciSequence {
    
    let includesZero: Bool
    let values: [UInt64]
    
    init(maxNumber: UInt64, includesZero: Bool) {
        self.includesZero = includesZero
        
        var sequence : [UInt64]
        
        if includesZero == true {
            sequence = [0,1]
        } else {
            sequence = [1,1]
        }
        
        while sequence.last <= maxNumber {
            let (lastNumber, didOveflow) = UInt64.addWithOverflow(sequence[sequence.count - 1], sequence[sequence.count - 2])
            if didOveflow == true {
                println("overFlow happened!")
                break
            }
            sequence.append(lastNumber)
        }
        
        sequence.removeLast()
        values = sequence
    }
    
    init(numberOfItemsInSequence: Int, includesZero: Bool) {
        
        self.includesZero = includesZero
        
        var sequence : [UInt64]
        
        if includesZero == true {
            sequence = [0,1]
        } else {
            sequence = [1,1]
        }
        let lastNumber = sequence.last
        let secondToLastNumber = sequence[sequence.count - 1]
        while sequence.count <= numberOfItemsInSequence {
            let (nextNumber, didOveflow) = UInt64.addWithOverflow(sequence[sequence.count - 1], sequence[sequence.count - 2])
            if didOveflow == true {
                println("overFlow happened")
                break
            }
            sequence.append(nextNumber)
        }
        sequence.removeLast()
        values = sequence
    }
}
